-- QUESTAO 1

-- a
DELIMITER $$;

DROP PROCEDURE IF EXISTS sp_Demite 

$$ CREATE PROCEDURE sp_Demite (funcionario_nome VARCHAR(100)) 
BEGIN
  UPDATE Funcionario
  SET status = 'Demitido'
  WHERE nome = funcionario_nome;
END $$ 

DELIMITER ;

CALL sp_Demite ('CARLOS MACEDO CERRI');

-- b
delimiter //
drop procedure if exists sp_SalvaDemitidos//
create procedure sp_SalvaDemitidos (in pcodFunc int)
begin
  set SQL_SAFE_UPDATES = 0;
  create table if not exists ExFuncionario(
  codFunc SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  nome VARCHAR(60) NOT NULL,
  salario DECIMAL(7,2) NOT NULL,
  categoria ENUM('Gerente', 'Técnico', 'Outros') NOT NULL DEFAULT 'Outros');
  insert into ExFuncionario(codFunc, nome, salario, categoria)
  select funcionario.codFunc, funcionario.nome, funcionario.salario, funcionario.categoria from funcionario
  where codFunc = pcodFunc;
end//
delimiter ;
call sp_SalvaDemitidos();
select * from ExFuncionario;
select * FROM Funcionario;
delimiter //
drop trigger if exists t_ExcluirFuncionario //
create trigger t_ExcluirFuncionario
before delete on funcionario
for each row
begin
  call sp_SalvaDemitidos (old.codFunc);
  delete from funcionario where codFunc = old.codFunc;
end //
delimiter ;
delete from funcionario where codFunc = 11;

-- c

delimiter //
drop procedure if exists sp_ExcluirDemitido//
create procedure sp_ExcluirDemitido ()
begin
  declare pStatus text;
  declare pcodFunc int;
  declare pNome text;
  declare pSalario float;
  declare pCateg text;
  set pStatus = (select status from funcionario where status = 'Demitido');
  set pcodFunc = (select codFunc from funcionario where status = 'Demitido');
  set pNome = (select nome from funcionario where status = 'Demitido');
  set pSalario = (select salario from funcionario where status = 'Demitido');
  set pCateg = (select categoria from funcionario where status = 'Demitido');
  if (pStatus = "Demitido") then
  set SQL_SAFE_UPDATES = 0;
  create table if not exists ExFuncionario(codFunc SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT, nome VARCHAR(60) NOT NULL,
  salario DECIMAL(7,2) NOT NULL, categoria ENUM('Gerente', 'Técnico', 'Outros') NOT NULL DEFAULT 'Outros');
  insert into ExFuncionario(codFunc, nome, salario, categoria) value (pcodFunc, pNome, pSalario, pCateg);
  delete from funcionario where status = 'Demitido';
  end if;
end//
delimiter ;
call sp_ExcluirDemitido();
select * FROM Funcionario;
select * from ExFuncionario;

-- d 
delimiter //
drop procedure if exists sp_AumentoSalarioCategoria//
create procedure sp_AumentoSalarioCategoria ()
begin
  update funcionario set salario = salario * (1 + (30/100)) where categoria = 'Técnico';
  update funcionario set salario = salario * (1 + (15/100)) where categoria = 'Gerente';
  update funcionario set salario = salario * (1 + (5/100)) where categoria = 'Outros';
end//
delimiter ;

-- e 
delimiter //
drop trigger if exists t_SalarioCategoria //
create trigger t_SalarioCategoria
  before update on funcionario
  for each row
begin
  if (new.salario < 1700.00 and new.categoria = 'Técnico') then
  signal sqlstate '45000' set message_text = 'Salario não permitido pela categoria!!!';
  else if (new.salario < 3500.00 and new.categoria = 'Gerente') then
  signal sqlstate '45000' set message_text = 'Salario não permitido pela categoria!!!';
  else if (new.salario < 970.00 and new.categoria = 'Outros') then
  signal sqlstate '45000' set message_text = 'Salario não permitido pela categoria!!!';
  end if;
  end if;
  end if;
end //
delimiter ;

-- QUESTAO 2
--a 
delimiter //
drop Procedure If Exists sp_LivrosNaoDisponiveis//
create Procedure sp_LivrosNaoDisponiveis() 
begin
  create table if not exists LivroNaoDisponivel(codLivro SMALLINT UNSIGNED PRIMARY KEY,
  nome VARCHAR(60) NOT NULL, precoVenda DECIMAL(5,2) UNSIGNED NOT NULL);
  insert into LivroNaoDisponivel (codLivro, nome, precoVenda)
  select * From (Select codLivro as codLivroInsert, nome, preco from Livro 
  where Livro.estoque = 0) as selection where not exists(select codLivro from LivroNaoDisponivel where codLivro =
  codLivroInsert);
end//
call sp_LivrosNaoDisponiveis();

-- b 
delimiter //
drop function If Exists f_VendasLivro//
create function f_VendasLivro
(nomeLivro varchar(60))
returns int
begin
  return (select count(v.codVenda) from Venda v join Livro l
  on v.codLivro = l.codLivro and l.nome = nomeLivro);
end//
select f_VendasLivro("Uma Mente Brilhante");

-- c
delimiter //
drop procedure If Exists sp_VendasLivro//
dreate procedure sp_VendasLivro
(in anoVenda int)
begin
  select count(v.codVenda) as quantidadeVenda, l.nome from Venda v join Livro l
  on v.codLivro = l.codLivro and v.ano = anoVenda group by l.nome;
end//
call sp_VendasLivro(2017);

-- d

-- e
DROP TRIGGER IF EXISTS t_VendaEstoque;

Delimiter //
CREATE TRIGGER t_VendaEstoque
AFTER INSERT on Venda
FOR EACH row 
BEGIN 
	DECLARE estoque int;
	SET estoque = (SELECT l.estoque from Livro l where l.codLivro = new.codLivro);
	if (estoque <= 0) then 
		signal sqlstate '45000' SET message_text = "livro fora de estoque";
	else
		Update Livro SET estoque=estoque-1 where Livro.codLivro=new.codLivro;
	end if;
end //
DELIMITER ;
	
Insert into Venda (precoVenda, ano, codLivro) VALUES(34.99, 2001, 1)
-- SELECT * FROM Venda v 
-- SELECT * FROM Livro l 


-- QUESTAO 3

-- a
CREATE USER secretaria@localhost IDENTIFIED BY 'secretaria_admin'

-- b
GRANT SELECT (matricula, nome) ON Aluno TO 'secretaria'@'localhost';
GRANT SELECT ON Disciplina TO 'secretaria'@'localhost';
GRANT SELECT (Matricula) ON Matricula TO 'secretaria'@'localhost';
FLUSH PRIVILEGES;

-- c
GRANT UPDATE(nome), INSERT(nome) ON Aluno TO 'secretaria'@'localhost';
GRANT UPDATE(nome), INSERT(nome) ON Disciplina TO 'secretaria'@'localhost';
FLUSH PRIVILEGES;

-- QUESTAO 4
-- a
Drop TRIGGER IF EXISTS validate_nota_insert;
Drop TRIGGER IF EXISTS validate_nota_update;

Delimiter //
-- Insert
CREATE TRIGGER validate_nota_insert
	BEFORE INSERT on Matricula
	FOR EACH ROW
	BEGIN
		IF (new.nota < 0 or new.nota > 10) then
			signal sqlstate '45000'
			Set Message_text = "Nota não permitida!!!";
		END IF;
  END //

-- Update
CREATE TRIGGER validate_nota_update
	BEFORE UPDATE on Matricula
	FOR EACH ROW
	BEGIN
		IF (new.nota < 0 or new.nota > 10) then
			signal sqlstate '45000'
			SET Message_text = "Nota não permitida!!!";
		END IF;
  END //
Delimiter ;

-- b
-- Com notas menores que 0 ou maiores que 10.
INSERT INTO Matricula (matricula, codDisciplina, nota) VALUES (1, 1, -1);
INSERT INTO Matricula (matricula, codDisciplina, nota) VALUES (1, 1, 10);

Update Matricula set nota = -1 where matricula = 1;
Update Matricula set nota = 11 where matricula = 1;

-- c

DROP TRIGGER IF EXISTS remove_disciplinas;

Delimiter //
CREATE TRIGGER remove_disciplinas
	BEFORE DELETE on Disciplina
	FOR EACH ROW
	BEGIN
		DELETE FROM matricula m
		WHERE m.codDisciplina = OLD.codDisciplina;
	END //
Delimiter ;

-- c
DELETE FROM Disciplina WHERE codDisciplina = 1;
